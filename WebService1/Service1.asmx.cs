﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace WebService1
{
    /// <summary>
    /// Summary description for Service1
    /// </summary>
    [WebService(Namespace = "http://meuWSjoaopedro.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Service1 : System.Web.Services.WebService
    {

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        public enum OperacaoTipo
        {
            Soma = 1,
            Subtracao = 2,
            multiplicacao = 3,
            divisao = 4,
        }

        [WebMethod (Description = "Faz uma operacao matematica simples com dois numeros.")]
        public double Operacao(double valor1, double valor2, OperacaoTipo operacaotipo) {
            double resultado;
            switch (operacaotipo)
            {
                case OperacaoTipo.Soma:
                    resultado = valor1 + valor2;
                    break;
                case OperacaoTipo.Subtracao:
                    resultado = valor1 - valor2;
                    break;
                case OperacaoTipo.divisao:
                    resultado = valor1 / valor2;
                    break;
                case OperacaoTipo.multiplicacao:
                    resultado = valor1 * valor2;
                    break;
                default:
                    resultado = 0;
                    break;
            }
 
            return resultado;
        }

    }
}